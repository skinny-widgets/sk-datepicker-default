# Skinny Widgets Datepicker for Default Theme


datepicker element

```
npm i sk-datepicker sk-datepicker-jquery --save
```

then add the following to your html

```html
<sk-config
    theme="default"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-default"
></sk-config>
<sk-datepicker id="myDatePicker">Date</sk-datepicker>
<script type="module">
    import { SkDatePicker } from './node_modules/sk-datepicker/index.js';

    customElements.define('sk-datepicker', SkDatePicker);

</script>
```
#### slots

**default (not specified)** - draws label for input

**label** - draws label for input

#### attributes

**open** - present if datepicker calendar widget is currently opened (for native datepicker only represents focus state)

**fmt** - date value format (default: 'm/d/Y')  

 * {Date} d [01-31]
 * {Short_Day_Name} D [Su, Mo, Tu, We, Th, Fr, Sa]
 * {Date} j [1-31]
 * {Full_day_name} l  [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
 * {Week_day_number} w 0=Sunday, 1=Monday, 2=Tuesday etc...
 * {Nth_day_of_year} z [1-365] except leap years
 * {Full_month_name} F [January, February, ...]
 * {Month_number} m [01-12]
 * {Month_name_stripped_to_three_letters} M [Jan, Feb, ...]
 * {Month_number} n [1-12]
 * {Days_in_current_month} t [28-31]
 * {Full_year} Y [1900, ...]
 * {Last_two_digits_of_a_year} y [01-99]
 * {Time_postfix} a [am|pm]
 * {Time_postfix} A [AM|PM]
 * {Hours_in_12h_format} g [1-12]
 * {Hours_in_24h_format} G [0-23]
 * {Hour_in_12h_format_with_padding} h [01-12]
 * {Hours_in_24h_format_with_padding} H [00-23]
 * {Minutes_with_padding} i [00-59]
 * {Seconds_with_padding} s [00-59]
 * {Timezone} Z 2 for GMT+2


#### template

id: SkDatePickerTpl