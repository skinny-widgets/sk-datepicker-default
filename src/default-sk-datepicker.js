
import { SkDatepickerImpl }  from '../../sk-datepicker/src/impl/sk-datepicker-impl.js';

export class DefaultSkDatepicker extends SkDatepickerImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'datepicker';
    }

    get input() {
        return this.comp.el.querySelector('input');
    }

    set input(el) {
        this._input = el;
    }

    get subEls() {
        return [ 'input' ];
    }

    static observedAttributes() {
        return ['value'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'value') {
            this.input.value = this.value;
        }
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.mountStyles();
    }

    bindEvents() {
        this.input.onchange = function(event) {
            this.comp.setAttribute('value', this.input.value);
            this.comp.dispatchEvent(new CustomEvent('change', { target: this.comp, bubbles: true, composite: true, detail: { }}));
            this.comp.dispatchEvent(new CustomEvent('skchange', { target: this.comp, bubbles: true, composite: true, detail: { }}));
        }.bind(this);
        this.input.oninput = function(event) {
            this.comp.setAttribute('value', this.input.value);
            this.comp.dispatchEvent(new CustomEvent('input', { target: this.comp, bubbles: true, composite: true, detail: { }}));
            this.comp.dispatchEvent(new CustomEvent('skinput', { target: this.comp, bubbles: true, composite: true, detail: { }}));
        }.bind(this);
        this.input.onfocus = function(event) {
            this.comp.setAttribute('open', '');
        }.bind(this);
        this.input.onblur = function(event) {
            this.comp.removeAttribute('open');
        }.bind(this);
        if (this.value) {
            this.input.value = this.value;
        }
    }

}
